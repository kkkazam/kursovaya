import socket
import os
bufferSize = 1024


def server_udp(server_ip, port):
    udp_server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)  # создали дейтаграмму для сокета
    udp_server_socket.bind((server_ip, port))
    print("UDP-Сервер включен\n\n")
    while True:
        bytesAddressPair = udp_server_socket.recvfrom(bufferSize)
        msg = bytesAddressPair[0]
        address = bytesAddressPair[1]
        client_msg = f"Сообщение от клиента: {msg}"
        clientIP = f"IP-адрес клиента: {address}"
        print(client_msg)
        print(clientIP)
        print("Отправка сообщение клиенту назад...")
        udp_server_socket.sendto(str.encode(client_msg.upper()), address)


def config_file():
    path_to_file = input("Введите полный путь до .cfg файла: ")
    if not os.path.exists(path_to_file):
        print("Такого пути не существует!")
        return 1
    if not os.path.isfile(path_to_file):
        print("Файл без расширения .cfg!")
        return 2
    f = open(path_to_file, 'r')
    return f.read()


def main():
    server_ip = input("Введите IP-адрес нужного интерфейса:")
    print("1-Загрузка вручную")
    print("2-Загрузка из конфигурационного файла")
    print("Введите q для выхода")
    cmd = input("Выберите операцию: ")
    while cmd != "exit" or "Exit" or "EXIT" or "q":
        if cmd == "1":
            port = int(input("Введите порт: "))
            if port <= 1024 or port > 49151:
                print("Неверный порт!")
                return
            server_udp(server_ip, port)
        if cmd == "2":
            port = int(config_file())
            if port <= 1024 or port > 49151:
                print("Неверный порт!")
                return
            print(port)
            server_udp(server_ip, port)
        #else:
            #print("Wrong operation!")
        cmd = input("Выберите операцию: ")
    return


main()