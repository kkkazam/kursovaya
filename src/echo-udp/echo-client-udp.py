import socket
import os
import sys
buffer_size = 1024


def client_udp(client_ip, client_port, message, server_ip_port):
    try:
        udp_client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        udp_client_socket.bind((client_ip, client_port))
        udp_client_socket.sendto(str.encode(message), server_ip_port)
        msgFromServer = udp_client_socket.recvfrom(buffer_size)
        msg = f"Сообщение от сервера: {msgFromServer[0]}"
        print(msg)
    except socket.error:
        print("Ошибка!")

def config_file():
    data = []
    path_to_file = input("Введите полный путь до .cfg файла: ")
    if not os.path.exists(path_to_file):
        print("Такого пути не существует!")
        return 1
    if not os.path.isfile(path_to_file):
        print("Файл без расширения .cfg!")
        return 2
    f = open(path_to_file, 'r')
    data = f.read().split()
    return data


def main():
    client_ip = input("Введите IP-адрес нужного интерфейса:")
    print("1-Загрузка вручную")
    print("2-Загрузка из конфигурационного файла")
    print("Введите q для выхода")
    cmd = input("Выберите операцию: ")
    while cmd != "exit" or "Exit" or "EXIT" or "q":
        if cmd == "1":
            udp_server_address = input("Введите IP-адрес UDP-сервера: ")
            server_port = int(input("Введите порт UDP-сервера: "))
            if server_port <= 1024 or server_port > 49151:
                print("Неверный порт!")
                return
            client_port = int(input("Введите порт клиента: "))
            if client_port <= 1024 or client_port > 49151:
                print("Неверный порт!")
                return
            message = input("Введите сообщение:")
            server_ip_port = (udp_server_address, server_port)
            client_udp(client_ip, client_port, message, server_ip_port)
        if cmd == "2":
            a_data = config_file()
            udp_server_address = a_data[0]
            server_port = int(a_data[1])
            if server_port <= 1024 or server_port > 49151:
                print("Неверный порт!")
                return
            client_port = int(a_data[2])
            if client_port <= 1024 or client_port > 49151:
                print("Неверный порт!")
                return
            message = input("Введите сообщение:")
            server_ip_port = (udp_server_address, server_port)
            client_udp(client_ip, client_port, message, server_ip_port)
        else:
            print("Неверная операция!")
        cmd = input("Выберите операцию: ")
    return


main()