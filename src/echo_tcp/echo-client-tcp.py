import socket
import threading
import os

#client_ip = "192.168.3.10"
buffer_size = 1024


def client_tcp(client_ip, client_port, ip_server, port_server):
    try:
        tcp_client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_client_socket.bind((client_ip, client_port))
        tcp_client_socket.connect((ip_server, port_server))

        thread1 = threading.Thread(target=client_writer, args=(tcp_client_socket, ip_server, port_server))
        thread1.daemon = True
        thread1.start()

        while True:
            message = tcp_client_socket.recv(buffer_size)
            decoded_message = message.decode()
            if not decoded_message:
                print('Серваер потерян\n')
                print('Остановка работы TCP-клиента\n')
                exit(0)
            print(f"""
                Сообщение от сервера:
                Адрес: {ip_server}:{port_server}
                Сообщение: {decoded_message}
                """)
    except TimeoutError:
        print("Время ожидания соединения вышло ")
    except ConnectionResetError:
        print("Соединение с сервером потеряно")


def client_writer(tcp_client_socket, server_ip, server_port):
    while True:
        message = input('Введите сообщение: ')
        coded_message = str.encode(message)
        print(f"""
        Отправка к: {server_ip}:{server_port}
        Отправленное сообщение: {message}
        """)
        try:
            tcp_client_socket.sendall(coded_message)
        except socket.error as e:
            print(f'Ошибка отправки сообщения: {e.errno}')


def config_file():
    data = []
    path_to_file = input("Укажите полный путь .cfg файла: ")
    if not os.path.exists(path_to_file):
        print("Такого пути не существует!")
        return 1
    if not os.path.isfile(path_to_file):
        print("Это не файл с расширением .cfg!")
        return 2
    f = open(path_to_file, 'r')
    data = f.read().split()
    return data


def main():
    try:
        client_ip = input("Введите IP-адрес нужного интерфейса: ")
        print("1-Загрузка вручную")
        print("2-Загрузка с конфигурационного файла")
        print('Введите "q" или "exit" для выхода')
        cmd = input("Выберите действие: ")
        while cmd != "exit" or "Exit" or "EXIT" or "q":
            if cmd == "1":
                ip_server = input("Введите IP-адрес TCP-сервера: ")
                port_server = int(input("Введите порт TCP-сервера: "))
                client_port = int(input("Введите порт клиента: "))
                client_tcp(client_ip, client_port, ip_server, port_server)
            if cmd == "2":
                a_data = config_file()
                ip_server = a_data[0]
                port_server = int(a_data[1])
                client_port = int(a_data[2])
                client_tcp(client_ip, client_port, ip_server, port_server)
            else:
                print("Неправильная операция!")
            cmd = input("Выберите действие: ")
        return
    except KeyboardInterrupt:
        print("Ошибка с вводом данных!")


main()