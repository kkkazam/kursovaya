import os
import socket
import select
import queue

buffer_size = 1024
max_connections = 10


def server_tcp(server_ip, port):
    print("Для закрытия TCP-сервера нажмите Ctrl+C")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setblocking(False)
    server.bind((server_ip, port))
    print(f"Сервер создан: IP - {server_ip}, port - {port}")
    server.listen(max_connections)  # Установка максимального количество подключений

    inputs = [server]
    outputs = []
    message = {}
    address = {}
    print("Ожидание подключения...")
    while True:
        readable, writable, exceptional = select.select(inputs, outputs, inputs)
        for client in readable:
            if client is server:
                # Если событие исходит от серверного сокета, то мы получаем новое подключение
                connection, client_address = client.accept()
                address[connection] = client_address[0]
                print(f"\nНовое подключение: {client_address[0]}:{client_address[1]}")
                connection.setblocking(False)
                inputs.append(connection)
                message[connection] = queue.Queue()
            else:
                # Если событие исходит не от серверного сокета, но сработало прерывание на наполнение входного буффера
                data = client.recv(buffer_size) #получение данных от клиента
                if data:
                    info = data.decode("utf-8")
                    print(f"Сообщение от клиента {address[client]} : {info}") #Вывод полученных данных на консоль
                    message[client].put(data)
                    if client not in outputs:  #Говорим о том, что мы будем еще и писать в данный сокет
                        outputs.append(client)
                else:
                    # Если данных нет, но событие сработало, то ОС нам отправляет флаг о полном прочтении ресурса и его закрытии
                    print(f"Клиент {address[client]} отключился")
                    # Очищаем данные о ресурсе и закрываем дескриптор
                    if client in outputs:
                        outputs.remove(client)
                    inputs.remove(client)
                    client.close()
                    if client in message:
                        del message[client]
                    if client in address:
                        del address[client]

        for client in writable:  # Данное событие возникает когда в буффере на запись освобождается место
            try:
                next_msg = message[client].get_nowait()
            except queue.Empty:
                outputs.remove(client)
            else:
                for src in address:
                    if src != client:
                        shipped = src.send(next_msg)
                        if not shipped:
                           print(f"Клиент {address[src]} потерян")
                           inputs.remove(src)
                           outputs.remove(src)
                           del message[src]
                           del address[src]
        for client in exceptional:
            print(f"Клиент {address[client]} потерян")
            inputs.remove(client)
            if client in outputs:
                outputs.remove(client)
            client.close()
            if client in message:
                del message[client]
            if client in address:
                del address[client]


def config_file():
    path_to_file = input("Введите полный путь до .cfg файла: ")
    if not os.path.exists(path_to_file):
        print("Такой путь с файлом не существует!")
        return 1
    if not os.path.isfile(path_to_file):
        print("Это не файл с расширением .cfg!")
        return 2
    f = open(path_to_file, 'r')
    return f.read()


def main():
    try:
        server_ip = input("Введите IP-адрес нужного интерфейса:")
        print("1-Введение данных вручную")
        print("2-Загрузка с файла")
        print('Для выхода введите "q" или "exit"')
        cmd = input("Выберите операцию: ")
        while cmd != "exit" or "Exit" or "EXIT" or "q":
            if cmd == "1":
                port = int(input("Введите порт: "))
                if port <= 1024 or port > 49151:
                    print("Неправильный порт!")
                    return
                server_tcp(server_ip, port)
            if cmd == "2":
                port = int(config_file())
                if port <= 1024 or port > 49151:
                    print("Неправильный порт!")
                    return
                print(port)
                server_tcp(server_ip, port)
            else:
                print("Неверная операция!")
            cmd = input("Выберите операцию: ")
        return
    except KeyboardInterrupt:
        print("Error!")


main()
