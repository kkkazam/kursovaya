import scapy.fields
import scapy.error
from scapy.all import *

from scapy.layers.inet import IP, ICMP


def sniffer(net_interface, count_pack):
    try:
        pkts = sniff(iface= net_interface, count=count_pack)
        ethernet_num = 0
        ip_num = 0
        arp_num = 0
        icmp_num = 0
        for p in pkts:
            s1 = p.name  # ethernet
            s2 = p.payload.name  # ip or arp
            s3 = p.payload.payload.name  #icmp
            if s1 == 'Ethernet':
                print('\nEthernet Num:' + str(ethernet_num))
                print('Source MAC:' + p.src)
                print('Destination MAC:' + p.dst)
                print('Вложенный протокол:' + p.payload.name)
                print('Размер данных в байтах:' + str(len(p)))
                ethernet_num = int(ethernet_num) + 1
                if s2 == 'IP':
                    print('\nIP Num: ' + str(ip_num))
                    print('Source IP:' + p[IP].src)
                    print('Destination IP:' + p[IP].dst)
                    print('TTL:' + str(p.ttl))
                    if 'ICMP' in p.summary():
                        print('Вложенный протокол:ICMP')
                    if 'UDP' in p.summary():
                        print('Вложенный протокол:UDP')
                    if 'TCP' in p.summary():
                        print('Вложенный протокол:TCP')
                    print('Размер данных в байтах:' + str(len(p[IP])))
                    ip_num = ip_num + 1

                    if s3 == 'ICMP':
                        print('\nICMP Num: ' + str(icmp_num))
                        print('Тип пакета:', p[ICMP].type)
                        print('Код пакета:', p[ICMP].code)
                        print('Размер данных в байтах:' + str(len(p[ICMP])))
                        icmp_num = icmp_num + 1

                if s2 == 'ARP':
                    print('\nARP Num:' + str(arp_num))
                    if 'who has' in p.summary():
                        print('ARP-Request')
                    if 'is at' in p.summary():
                        print('ARP-Reply')
                    arp_num = arp_num + 1
        print('\n\n\n')
        pkts.summary()
        print('Ethernet:' + str(ethernet_num) + '\nIP:'+ str(ip_num) + '\nICMP:' + str(icmp_num) + '\nARP:' + str(arp_num) )
    except OSError:
        print("Нет такого интерфейса! или запуск без прав администратора(суперпользователя)")
    except scapy.error.Scapy_Exception:
        print("Error!")


if __name__=='__main__':
    try:
        print(ls("ARP"))
        print(ifaces)
        net_interface = input("Выберите сетевой интерфейс для захвата трафика:")
        count_pack = int(input("Введите число перехваченных пакетов: "))
        sniffer(net_interface, count_pack)
    except ValueError:
        print("Неверно введены данные!")
