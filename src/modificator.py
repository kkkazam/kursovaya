from scapy.all import *
from scapy.layers.inet import Ether,IP, UDP, TCP
import queue
import re
import os
import threading
import netifaces
import time
import typer


ip_a = '192.168.3.10'
ip_b = '192.168.3.20'
ip_c = '192.168.3.30'
ip_d = '192.168.3.40'

client_ip_b = ''
client_port_b = 0
client_ip_a = ''
client_port_a = 0
server_ip_d = ''
server_port_d = 0

file_change = ''
file_lock = ''

block_table = threading.Lock() #блокирование потока
default_ttl = 300 #ttl по-умолчанию

iface1 = "ens33" #имя первого сетевого интерфейса
iface2 = "ens38" #имя второго сетевого интерфейса
list_ifaces = [iface1, iface2] #лист с именами сетевых интерфейсов

mac_addr1 = netifaces.ifaddresses(iface1)[netifaces.AF_LINK][0]['addr'] #mac-адрес первого сетевого интерфейса
mac_addr2 = netifaces.ifaddresses(iface2)[netifaces.AF_LINK][0]['addr'] #mac-адрес второго сетевого интерфейса
list_mac_addresses = [mac_addr1, mac_addr2] #лист с mac-адресами сетевых интерфейсов

conf.verb = 0 #для отключения вывода функции sendp с точкой

info_dict = {iface1: [], iface2: []} #словарь с интерфесами и портами


mac_a = "00:0c:29:ac:89:56"
mac_b = "00:0c:29:48:39:b8"
mac_d = "00:0c:29:b5:72:20"

def read_file(filename):
    global client_ip_b, client_ip_a, server_ip_d
    with open(filename, 'r') as file:
        client_ip_b, port_b = file.readline().split()
        client_ip_a, port_a = file.readline().split()
        server_ip_d, port_d = file.readline().split()
    return [client_ip_b, port_b, client_ip_a, port_a, server_ip_d, port_d]




class Switch():
    def __init__(self):
        self.switch_table = {}

    def mutex_flag(self):
        block_table.acquire()

    def append_table(self, port, mac_address):
        self.mutex_flag()
        if port in self.switch_table:
            self.switch_table[port][mac_address] = time.time()
        else:
            self.switch_table[port] = {}
            self.switch_table[port][mac_address] = time.time()
        block_table.release()

    def check_time(self):
        self.mutex_flag()
        flag = True
        while flag:
            flag = False
            for ports in self.switch_table:
                for mac_addrs in self.switch_table[ports]:
                    if int(time.time() - self.switch_table[ports][mac_addrs]) >= default_ttl:
                        del self.switch_table[ports][mac_addrs]
                        if not self.switch_table[ports]:
                            del self.switch_table[ports]
                        flag = True
                    if flag:
                        break
                if flag:
                    break
        block_table.release()

    def find_address(self, mac_addr):
        self.mutex_flag()
        for ports in self.switch_table:
            if mac_addr in self.switch_table[ports]:
                block_table.release()
                return ports
        block_table.release()
        return None


table = Switch()
mac_queue = queue.Queue()


def change_packet(packet):


    # if "IP" in packet and "UDP" in packet:
    #     if packet[1].src == client_ip_a and packet[2].sport == client_port_a and packet[1].dst == server_ip_d and \
    #             packet[2].dport == server_port_d:
    #         mac_queue.put([packet[0].src, packet[1].src, packet[2].sport])
    #         packet[1].src = client_ip_b
    #         packet[2].sport = client_port_b
    #         #wrpcap(file_change, packet, append=True)
    #     elif packet[1].src == server_ip_d and packet[2].sport == server_port_d and packet[1].dst == client_ip_b and \
    #             packet[2].dport == client_port_b:
    #         mac_dst, ip_dst, port_dst = mac_queue.get()
    #         packet[0].dst = mac_dst
    #         packet[1].dst = ip_dst
    #         packet[2].dport = port_dst
    #         #wrpcap(file_change, packet, append=True)
    #
    #     if packet[1].src == client_ip_a and packet[2].sport == client_port_a and packet[1].dst == server_ip_d and \
    #             packet[2].dport == server_port_d:
    #         mac_queue.put([packet[0].src, packet[1].src, packet[2].sport])
    #         wrpcap(file_change, packet, append=True)
    #     packet[1].ttl = 128
    #     del packet[1].chksum
    #     del packet[2].chksum
    #     packet = packet.__class__(bytes(packet))
    #
    #
    # if "TCP" in packet: #5.3
    #     if not ((packet[1].src == ip_a and packet[1].dst == ip_c) or (packet[1].src == ip_c and packet[1].dst == ip_a)):
    #         wrpcap(file_lock, packet, append=True)
    #         return None
    # return packet
    # if packet.haslayer(TCP):
    #     if packet['IP'].src == client_ip_a and packet['TCP'].sport == client_port_a and packet['IP'].dst == server_ip_d and \
    #          packet['TCP'].dport == server_port_d:
    #         mac_queue.put([packet[Ether].src])
    #         print(packet[Ether].src)
    #         packet['IP'].src = client_ip_b
    #         packet['TCP'].src = client_port_b
    #         #packet['TCP'].seq = packet['TCP'].seq + 1
    #         #packet['TCP'].ack = packet['TCP'].ack + 1
    #         del packet[1].chksum
    #         del packet[2].chksum
    #         packet = packet.__class__(bytes(packet))
    #     if packet['IP'].src == server_ip_d and packet['TCP'].sport == server_port_d and packet['IP'].dst == client_ip_b and \
    #          packet['TCP'].dport == client_port_b:
    #         mac_dst = mac_queue.get()
    #         print(mac_dst)
    #         packet['IP'].src = client_ip_a
    #         packet['TCP'].src = client_port_a
    #         #packet['TCP'].seq = packet['TCP'].seq + 1
    #         #packet['TCP'].ack = packet['TCP'].ack + 1
    #         packet[Ether].dst = mac_dst
    #         del packet[1].chksum
    #         del packet[2].chksum
    #         packet = packet.__class__(bytes(packet))

    if flag11 == '1':
        if packet.haslayer(TCP):
            if packet[IP].src == client_ip_a and packet[TCP].sport == client_port_a and packet[IP].dst == server_ip_d and \
                    packet[TCP].dport == server_port_d:
                packet[Ether].src = mac_b
                packet[IP].src = client_ip_b
                packet[TCP].sport = client_port_b

            elif packet[IP].src == server_ip_d and packet[TCP].sport == server_port_d and packet[IP].dst == client_ip_b and \
                    packet[TCP].dport == client_port_b:
                packet[Ether].dst = mac_a
                packet[IP].dst = client_ip_a
                packet[TCP].dport = client_port_a
            del packet[1].chksum
            del packet[2].chksum
            packet = packet.__class__(bytes(packet))
        return packet

    if flag11 == '2':
        if packet.haslayer(UDP):
            packet[1].ttl = 128
            wrpcap(file_change, packet, append=True)
            del packet[1].chksum
            del packet[2].chksum
            packet = packet.__class__(bytes(packet))

    if flag11 == '3':
        if packet.haslayer(TCP): #5.3
            if not ((packet[1].src == ip_a and packet[1].dst == ip_c) or (packet[1].src == ip_c and packet[1].dst == ip_a)):
                wrpcap(file_lock, packet, append=True)
                return None
    #return packet


def parse_packet(packet):
    com_port = packet.sniffed_on
    source_port = table.find_address(packet[0].src)

    if ((source_port is None) or source_port == com_port) and not (packet[0].src in list_mac_addresses):
        table.append_table(com_port, packet[0].src)
        packet = change_packet(packet)
        if packet is None:
            return None
        if packet[0].dst == 'ff:ff:ff:ff:ff:ff':
            for i in list_ifaces:
                if i != com_port:
                    if packet is not None:
                        sendp(packet, i)
        else:
            port_dst = table.find_address(packet[0].dst)
            if port_dst is not None:
                if port_dst != com_port:
                    if packet is not None:
                        sendp(packet, port_dst)
            else:
                if packet is not None:
                    for i in list_ifaces:
                        if i != com_port:
                            sendp(packet, i)


def sniff_packets(iface_i):
    sniff(iface=iface_i, prn=parse_packet)


def check_time():
    while True:
        time.sleep(5)
        table.check_time()


def main(file_conf: str = typer.Argument('', help='Введите путь конфигурационного файла'),
         file_ch: str = typer.Argument('', help='Введите полный путь до change.pcap'),
         file_l: str = typer.Argument('', help='Enter with configure до lock.pcap'),
         flag1: int = typer.Argument('', help='Enter flag for launch')):
    try:
        global file_change, file_lock, flag11
        file_change = file_ch
        file_lock = file_l
        flag11 = flag1
        parameters = read_file(file_conf)


        global client_port_b, client_port_a, server_port_d
        client_port_b = int(parameters[1])
        client_port_a = int(parameters[3])
        server_port_d = int(parameters[5])

        print("Выберитие вариант модификации:\n"
              "1-Модификация пакетов TCP узла A на B с сервером D\n"
              "2-Увеличение TTL до максимального для всех пакетов UDP + запись PCAP файла\n"
              "3-Разрешение работы только между узлами A и C по протоколу TCP + запись PCAP файла\n")
        flag11 = input()

        print('Модификатор запущен. Для выхода введите Ctrl+C\n')
        for i in list_ifaces:
            thread_sniff = threading.Thread(target=sniff_packets, args=(i,))
            thread_sniff.daemon = True
            thread_sniff.start()

        check_time()

    except KeyboardInterrupt:
        print('\nКонец работы')


if __name__ == "__main__":
    typer.run(main)
