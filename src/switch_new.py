from scapy.all import *
import threading
import statistics
import netifaces
import tabulate
import time

block_table = threading.Lock() #блокирование потока
default_ttl = 300 #ttl по-умолчанию

iface1 = "ens33" #имя первого сетевого интерфейса
iface2 = "ens38" #имя второго сетевого интерфейса
list_ifaces = [iface1, iface2] #лист с именами сетевых интерфейсов

mac_addr1 = netifaces.ifaddresses(iface1)[netifaces.AF_LINK][0]['addr'] #mac-адрес первого сетевого интерфейса
mac_addr2 = netifaces.ifaddresses(iface2)[netifaces.AF_LINK][0]['addr'] #mac-адрес второго сетевого интерфейса
list_mac_addresses = [mac_addr1, mac_addr2] #лист с mac-адресами сетевых интерфейсов

conf.verb = 0 #для отключения вывода функции sendp с точкой

info_dict = {iface1: [], iface2: []} #словарь с интерфесами и портами


class Switch():
    def __init__(self):
        self.switch_table = {} #словарь {port:mac}

    def mutex_flag(self): #функция блокировки потоков
        block_table.acquire()

    def append_table(self, port, mac_address): #функция добавления записи в таблицу
        self.mutex_flag()
        if port in self.switch_table:
            self.switch_table[port][mac_address] = time.time()
        else:
            self.switch_table[port] = {}
            self.switch_table[port][mac_address] = time.time()
        block_table.release()

    def check_time(self):
        self.mutex_flag()
        flag = True
        while flag:
            flag = False
            for ports in self.switch_table:
                for mac_addrs in self.switch_table[ports]:
                    if int(time.time() - self.switch_table[ports][mac_addrs]) >= default_ttl:
                        del self.switch_table[ports][mac_addrs]
                        if not self.switch_table[ports]:
                            del self.switch_table[ports]
                        flag = True
                    if flag:
                        break
                if flag:
                    break
        block_table.release()

    def find_address(self, mac_addr):
        self.mutex_flag()
        for ports in self.switch_table:
            if mac_addr in self.switch_table[ports]:
                block_table.release()
                return ports
        block_table.release()
        return None

    def print_table(self):
        self.mutex_flag()
        info_list = []
        info_list2 = []
        print()
        print('#' * 50)
        for ports in self.switch_table:
            for mac_address in self.switch_table[ports]:
                info_list2.append((ports,
                                   mac_address,
                                   default_ttl-int(time.time() - self.switch_table[ports][mac_address])))

        print(tabulate.tabulate(info_list2,
                                headers=(
                                        'Порт: ',
                                        'MAC:',
                                        'Время: ',
                                        ), ))
        print('-' * 50)
        print('Информация о пакете:')
        for i in list_ifaces:
            if len(info_dict[i]) > 0:
                info_list.append((i,
                                  len(info_dict[i]),
                                  statistics.mean(info_dict[i]),
                                  max(info_dict[i]),
                                  ))
        print('-' * 50)
        print(tabulate.tabulate(info_list,
                                headers=(
                                        'Порт: ',
                                        'Количество пакетов:',
                                        'Среднее время обработки: ',
                                        'Максимальное время обработки: ',
                                    ), ))
        print('#' * 50)
        block_table.release()


table = Switch()


def parse_packet(packet):
    start_time = time.time()
    com_port = packet.sniffed_on
    source_port = table.find_address(packet[0].src)
    if ((source_port is None) or source_port == com_port) and not (packet[0].src in list_mac_addresses):
        table.append_table(com_port, packet[0].src)

        if packet[0].dst == 'ff:ff:ff:ff:ff:ff':
            for i in list_ifaces:
                if i != com_port:
                    sendp(packet, i)
        else:
            port_dst = table.find_address(packet[0].dst)
            if port_dst is not None:
                if port_dst != com_port:
                    sendp(packet, port_dst)
            else:
                for i in list_ifaces:
                    if i != com_port:
                        sendp(packet, i)
        end_time = time.time()
        calculated_time = end_time - start_time
        table.mutex_flag()
        info_dict[com_port].append(calculated_time)
        block_table.release()


def sniff_packets(iface_i):
    sniff(iface=iface_i, prn=parse_packet)


def check_time():
    while True:
        time.sleep(5)
        table.check_time()
        table.print_table()


def main():
    try:
        global default_ttl
        default_ttl = int(input('Введите TTL: '))
        print('Коммутатор включен, для отмены работы введите Ctrl+C\n')
        for i in list_ifaces:
            thr_sniff = threading.Thread(target=sniff_packets, args=(i,))
            thr_sniff.daemon = True
            thr_sniff.start()
        check_time()
    except KeyboardInterrupt:
        print("Ошибка!")


main()